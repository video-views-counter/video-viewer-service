package com.hsuk.video.viewer.service.api.validator;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

@Slf4j
public class UrlValidatorTest {

    private UrlValidator urlValidator = new UrlValidator();

    @Test
    public void testValid() {
        String url = urlValidator.encode(1);

        log.info("Encoded url:" + url);
        Assert.assertTrue(urlValidator.validate(url).isValid());
    }

    @Test
    public void testInvalidRandom() {
        HashUtils utils = new HashUtils("owj0p5U9qpY", 12);

        String hash = utils.encode(179424673, 100, 1);

        Assert.assertFalse(urlValidator.validate(hash).isValid);
    }

    @Test
    public void testInvalidSecret() {
        HashUtils utils = new HashUtils("owj0p5U9qpY", 12);

        String hash = utils.encode(17942467, 1190494760, 1);

        Assert.assertFalse(urlValidator.validate(hash).isValid);
    }

    @Test
    public void testInvalidSalt() {
        HashUtils actualHashUtil = new HashUtils("owj0p5U9qpY", 12);
        HashUtils utils = new HashUtils("owj0p5U9qp", 12);

        String hash = utils.encode(179424673, 1190494760, 1);
        String actualHash = actualHashUtil.encode(179424673, 1190494760, 1);
        log.info("Encoded: " + hash);
        log.info("Actual: " + actualHash);

        Assert.assertFalse(urlValidator.validate(hash).isValid);
        Assert.assertTrue(urlValidator.validate(actualHash).isValid);
    }

    @Test
    public void testDecode() {
        HashUtils actualHashUtil = new HashUtils("owj0p5U9qpY", 12);
        String validUrl = "BnRQBnLiYqX0RGCV";

        long[] decoded = actualHashUtil.decode(validUrl);

        Assert.assertEquals(3, decoded.length);
    }

    @Test
    public void testInvalidDecode() {
        HashUtils actualHashUtil = new HashUtils("owj0p5U9qp", 12);
        String validUrl = "BnRQBnLiYqX0RGCV";

        long[] decoded = actualHashUtil.decode(validUrl);

        Assert.assertEquals(0, decoded.length);
    }
}
