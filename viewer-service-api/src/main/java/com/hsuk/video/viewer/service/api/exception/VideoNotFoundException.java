package com.hsuk.video.viewer.service.api.exception;

public class VideoNotFoundException extends RuntimeException {

    public VideoNotFoundException(String msg) {
        super(msg);
    }
}
