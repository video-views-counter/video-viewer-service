package com.hsuk.video.viewer.service.api.validator;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class UrlValidator {

    @Data
    @AllArgsConstructor
    public static class ValidationResult {
        long videoId;
        boolean isValid;
    }

    private long secret = 179424673;
    private HashUtils hashUtils;
    private final ValidationResult INVALID = new ValidationResult(-1, false);

    public UrlValidator() {
        hashUtils = new HashUtils("owj0p5U9qpY", 12);
    }

    public ValidationResult validate(String url) {
        try {

            long[] info = hashUtils.decode(url);

            if (info.length < 3) {
                return INVALID;
            }

            long givenSecret = info[0];
            long randomNumber = info[1];
            long videoId = info[2];

            if (givenSecret != secret || randomNumber < 1190494759 || randomNumber > 29276448013L) {
                return INVALID;
            }

            return new ValidationResult(videoId, true);
        } catch (Exception ex) {
            return INVALID;
        }

    }

    public String encode(long videoId) {

        long random = ThreadLocalRandom.current().nextLong(1190494759, 29276448013L);
        return hashUtils.encode(secret, random, videoId);
    }
}
