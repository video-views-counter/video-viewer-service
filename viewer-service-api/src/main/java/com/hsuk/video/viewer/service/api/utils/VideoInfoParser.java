package com.hsuk.video.viewer.service.api.utils;

import com.hsuk.video.viewer.service.api.validator.UrlValidator;
import com.hsuk.video.viewer.service.dao.dto.VideoInfoDto;
import com.hsuk.video.viewer.service.dao.service.VideoViewsDao;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Component
public class VideoInfoParser {

    private UrlValidator urlValidator;
    private VideoViewsDao videoViewsDao;
    private Random random = new SecureRandom();

    @Autowired
    public VideoInfoParser(UrlValidator urlValidator, VideoViewsDao videoViewsDao) {
        this.urlValidator = urlValidator;
        this.videoViewsDao = videoViewsDao;
    }

    //@PostConstruct
    public void parseCSVFile() {
        String fileName = "/home/kadhikari/Downloads/CAvideos.csv";

        List<VideoInfoDto> buffer = new ArrayList<>();
        CSVReader reader = null;

        try {
            reader = new CSVReader(new FileReader(fileName));

            String[] videoInfo;
            long counter = 0;
            while ((videoInfo = reader.readNext()) != null) {

                if (counter == 0) {
                    counter++;
                    continue;
                }

                if (videoInfo.length < 15) {
                    continue;
                }

                VideoInfoDto videoInfoDto = new VideoInfoDto();
                videoInfoDto.setVideoName(videoInfo[2]);
                String videoDescription = videoInfo[15];
                if (videoDescription.length() > 1024) {
                    videoInfoDto.setVideoDescription(videoDescription.substring(0, 1023));
                } else {
                    videoInfoDto.setVideoDescription(videoDescription);
                }

                videoInfoDto.setVideoId(counter);
                videoInfoDto.setVideoUrl(urlValidator.encode(counter));
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                videoInfoDto.setUploaderId(random.nextInt() & Integer.MAX_VALUE);
                try {
                    Date utcDateTime = formatter.parse(videoInfo[5]);
                    videoInfoDto.setUploadDate(utcDateTime);

                } catch (ParseException e) {
                    log.error("Exception while changing date: {}", e.getMessage());
                }

                buffer.add(videoInfoDto);
                counter++;

                if (buffer.size() > 100) {
                    videoViewsDao.saveVideoInfos(buffer);
                    buffer.clear();
                }
            }

            videoViewsDao.saveVideoInfos(buffer);
        } catch (FileNotFoundException e) {
            log.error("File not found: {}", e.getMessage());
        } catch (IOException ie) {
            log.error("Exception: {}", ie.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error("Exception: {}", e.getMessage());
                }
            }
        }

    }
}