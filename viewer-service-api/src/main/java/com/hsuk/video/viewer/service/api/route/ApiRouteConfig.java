package com.hsuk.video.viewer.service.api.route;

import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import com.hsuk.video.viewer.service.api.handler.VideoRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.http.javadsl.server.PathMatchers.segment;

@Component
public class ApiRouteConfig extends AllDirectives implements RouteConfig {


    private final Duration searchReqTimeout;
    private Environment environment;
    private VideoRequestHandler videoRequestHandler;

    @Autowired
    public ApiRouteConfig(VideoRequestHandler requestHandler, Environment env) {
        this.environment = env;
        this.videoRequestHandler = requestHandler;
        searchReqTimeout = Duration.create(environment.getProperty("videoservice.search.timeout", Long.class, 5L),
                TimeUnit.SECONDS);
    }

    /**
     * Master route creator
     */
    @Override
    public Route createRoutes() {
        return concat(pathPrefix("video-viewer", () ->
                concat(
                        playVideo(),
                        getVideoInfo(),
                        getVideoViewsCount()
                )
        ));
    }

    private Route playVideo() {

        return get(() -> pathPrefix("playVideo",
                () -> pathPrefix(segment(),
                        videoUrl ->
                                parameter("sessionId",
                                        sessionId ->
                                                parameter("userId",
                                                        userId ->
                                                                withRequestTimeout(searchReqTimeout,
                                                                        () -> videoRequestHandler.doPlayVideo(videoUrl, sessionId, userId)))))));


    }

    private Route getVideoInfo() {
        return get(() -> pathPrefix("getInfo",
                () -> pathPrefix(segment(),
                        videoId -> withRequestTimeout(searchReqTimeout,
                                () -> videoRequestHandler.doGetVideoInfo(videoId)))));
    }

    private Route getVideoViewsCount() {

        return get(() -> pathPrefix("viewsCount",
                () -> pathPrefix(segment(),
                        videoUrl ->
                                parameter("fromDate",
                                        fromDate ->
                                                parameter("toDate",
                                                        toDate ->
                                                                withRequestTimeout(searchReqTimeout,
                                                                        () -> videoRequestHandler.getViewsCount(videoUrl, fromDate, toDate)))))));


    }

}