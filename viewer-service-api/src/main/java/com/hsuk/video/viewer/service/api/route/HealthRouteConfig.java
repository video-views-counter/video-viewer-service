package com.hsuk.video.viewer.service.api.route;

import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import org.springframework.stereotype.Component;

@Component
public class HealthRouteConfig extends AllDirectives implements RouteConfig {

	public Route createRoutes() {
		return 
		        concat(pathPrefix("health", () -> health()));
	}
	
	// handle GET /health and /health/
    private Route health() {
    	
    	return pathEndOrSingleSlash(() -> complete(StatusCodes.OK, "{\"status\":\"UP\"}"));
    }

}
