package com.hsuk.video.viewer.service.api.service;

import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.hsuk.video.viewer.service.api.service.kafka.VideoViewMessage;
import com.hsuk.video.viewer.service.api.service.kafka.KafkaCallback;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Service class to write into kafka
 *
 * @author Sumit
 */
@Slf4j
@Service
public class KafkaService {
    private String topic;
    private Producer<String, VideoViewMessage> producer;
    private DynamicPropUtil dynamicPropUtil;

    @Autowired
    public KafkaService(DynamicPropUtil dynamicPropUtil) {
        this.dynamicPropUtil = dynamicPropUtil;
    }

    @PostConstruct
    public void setup() {
        try (InputStream input = KafkaService.class.getClassLoader().getResourceAsStream("kafka.properties")) {
            Properties props = new Properties();
            props.load(input);
            props.put("bootstrap.servers", dynamicPropUtil.getStr("bootstrap.servers"));
            topic = props.getProperty("topic");
            producer = new KafkaProducer<>(props);
            log.info("kafka service setup successful");
            log.info("this is property " + props.toString());
            log.info("kafka service setup" + producer.metrics().toString());
        } catch (IOException e) {
            log.error("Error reading property file", e);
        }
    }

    void writeIntoPipeline(VideoViewMessage videoInfo) {
        log.debug("writing into pipeline "+videoInfo.toString());
        long currentTimestamp = videoInfo.getTimestamp();
        String key = String.valueOf(videoInfo.getVideoId());
        producer.send(new ProducerRecord<String, VideoViewMessage>(topic, key, videoInfo), new KafkaCallback(currentTimestamp, key));
    }
}