package com.hsuk.video.viewer.service.dao.entity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;

public class EntityListener {
    private static String APP_IP;

    static {
        try {
            APP_IP = Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            // Ignore exception
            APP_IP = "Unknown";
        }
    }

    @PrePersist
    protected void prePersist(AbstractEntity abstractEntity) {
        abstractEntity.createdAt = new Date();
        abstractEntity.createdBy = APP_IP;
    }

    @PreUpdate
    protected void onUpdate(AbstractEntity abstractEntity) {
        abstractEntity.modifiedAt = new Date();
        abstractEntity.modifiedBy = APP_IP;
    }
}
