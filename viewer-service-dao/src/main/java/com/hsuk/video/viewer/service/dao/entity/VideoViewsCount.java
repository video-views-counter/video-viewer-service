package com.hsuk.video.viewer.service.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "videoviews_count")
public class VideoViewsCount extends AbstractEntity {

    @EmbeddedId
    private VideoViewsPK videoViewsPK;

    @Column(name = "views_count")
    private long viewsCount;
}