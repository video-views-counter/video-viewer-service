package com.hsuk.video.viewer.service.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "video_info", indexes =
        {@Index(name = "video_url_uniq_Idx", columnList = "video_url", unique = true)})
public class VideoInfo extends AbstractEntity {

    @Id
    @Column(name = "video_id")
    private long videoId;

    @Column(name = "video_url")
    private String videoUrl;

    @Column(name = "video_name")
    private String videoName;

    @Column(name = "video_description", length = 1024)
    private String videoDescription;

    @Column(name = "upload_date")
    private Date uploadDate;

    @Column(name = "uploader_id")
    private long uploaderId;
}
