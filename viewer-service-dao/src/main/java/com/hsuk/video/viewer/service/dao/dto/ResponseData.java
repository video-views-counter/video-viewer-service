package com.hsuk.video.viewer.service.dao.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public abstract class ResponseData implements Serializable {

    public ResponseData() {
        this.version = getClass().getPackage().getImplementationVersion();
    }

    private String version;

    private String message;

    private String error;

    private transient Date timeStamp;
}
