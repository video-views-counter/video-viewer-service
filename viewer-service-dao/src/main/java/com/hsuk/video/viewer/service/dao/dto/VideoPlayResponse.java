package com.hsuk.video.viewer.service.dao.dto;

import lombok.Data;

@Data
public class VideoPlayResponse extends ResponseData {

    private String videoUrl;
    private String videoName;
    private String description;
}
