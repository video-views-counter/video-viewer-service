package com.hsuk.video.viewer.service.dao.redis.repository;

import com.hsuk.video.viewer.service.dao.dto.VideoInfoDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedisVideoRepository extends CrudRepository<VideoInfoDto, String> {

}