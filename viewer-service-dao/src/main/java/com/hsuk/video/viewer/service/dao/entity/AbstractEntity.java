package com.hsuk.video.viewer.service.dao.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(EntityListener.class)
public abstract class AbstractEntity implements Serializable {

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdAt;

    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date modifiedAt;

    @Column(name = "created_by", nullable = false)
    protected String createdBy;

    @Column(name = "modified_by")
    protected String modifiedBy;

}
