package com.hsuk.video.viewer.service.dao.config;

import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableJpaAuditing
@EnableTransactionManagement
@ComponentScan(basePackageClasses = DynamicPropUtil.class)
@EnableJpaRepositories(
        basePackages = {"com.hsuk.video.viewer.service.dao.repository", "com.hsuk.platform.utils.dynaconfig.repository"}
)
public class PersistenceConfig {

    private DynamicPropUtil dynamicPropUtil;

    @Autowired
    public PersistenceConfig(DynamicPropUtil dynamicPropUtil) {
        this.dynamicPropUtil = dynamicPropUtil;
    }

    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(mysqlPrimaryDatasource());
        em.setPackagesToScan("com.hsuk.video.viewer.service.dao.entity", "com.hsuk.platform.utils.dynaconfig.entity");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(dynamicPropUtil.getBool("hibernate.ddl.auto", false));
        vendorAdapter.setShowSql(dynamicPropUtil.getBool("hibernate.show.sql", false));
        em.setJpaVendorAdapter(vendorAdapter);

        return em;
    }

    @Bean(name = "mysqlPrimaryDatasource")
    public DataSource mysqlPrimaryDatasource() {

        HikariConfig config = new HikariConfig();
        String dbHost = dynamicPropUtil.getStr("db.primary.host");
        String dbPort = dynamicPropUtil.getStr("db.primary.port");
        String dbUser = dynamicPropUtil.getStr("db.rw.username");
        String dbPass = dynamicPropUtil.getStr("db.rw.password");
        String dbName = dynamicPropUtil.getStr("db.name");

        if (StringUtils.isAnyBlank(dbHost, dbPort, dbUser, dbPass, dbName)) {
            log.error("Mandatory DB configuration properties not defined Properly. Please check your configuration files.");
            throw new IllegalArgumentException("Mandatory DB configuration properties not defined.");
        }

        String dbUrl = String.format("jdbc:mysql://%s:%s/%s?serverTimezone=EST5EDT", dbHost, dbPort, dbName);
        config.setJdbcUrl(dbUrl);
        config.setUsername(dbUser);
        config.setPassword(dbPass);

        config.setPoolName(dynamicPropUtil.getStr("poolName", "Hikari-CP-WC"));
        config.setMaximumPoolSize(dynamicPropUtil.getInt("maximumPoolSize", 50));
        config.setMinimumIdle(dynamicPropUtil.getInt("minimumPoolSize", 10));
        config.setAutoCommit(true);
        config.setConnectionTimeout(dynamicPropUtil.getInt("connectionTimeout", 45000));

        config.addDataSourceProperty("cachePrepStmts", dynamicPropUtil.getStr("db.cache.prepstmt", "true"));
        config.addDataSourceProperty("prepStmtCacheSize", dynamicPropUtil.getStr("db.stmt.cache.size", "256"));
        config.addDataSourceProperty("prepStmtCacheSqlLimit", dynamicPropUtil.getStr("db.stmt.cache.sql.limit", "2048"));

        return new HikariDataSource(config);

    }

}
