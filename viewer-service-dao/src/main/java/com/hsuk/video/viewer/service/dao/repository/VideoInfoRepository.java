package com.hsuk.video.viewer.service.dao.repository;

import com.hsuk.video.viewer.service.dao.entity.VideoInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VideoInfoRepository extends JpaRepository<VideoInfo, Long> {

}
